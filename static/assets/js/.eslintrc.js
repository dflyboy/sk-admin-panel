module.exports = {
    "env": {
        "browser": true,
        "jquery": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2017
    },
    "globals": {
        'M': 'readonly',
        'initRegister': 'readonly',
        'i18next': 'readonly',
        'jqueryI18next': 'readonly',
        'Localize': 'readonly',
        'i18nextXHRBackend': 'readonly'
    },
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};