/**
 * called after localization done
 */
function initPage() {

    const ctx = $('#revChart');

    const elems = document.querySelectorAll('.datepicker');
    M.Datepicker.init(elems, {
        format: 'dd.mm.yyyy'
    });

    $('#filterForm').submit(function (e) {
        e.preventDefault();
        showChart($('#input_start').val(), $('#input_end').val());
    });

    function showChart(start, end) {
        /**
         * 
         * @param {Object[]} data 
         */
        function showRevenues(data) {
            var dates = [],
                sums = [];
            data.forEach(entry => dates.push(moment(entry.day).format('DD.MM.YY')));
            data.forEach(entry => sums.push(entry.total));


            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dates,
                    datasets: [{
                        label: i18next.t('stats:revenue'),
                        data: sums,
                        backgroundColor: 'rgba(238, 110, 115, 0.2)',
                        borderColor: 'rgba(238, 110, 115, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value, index, values) {
                                    return value + ' €';
                                }
                            }
                        }]
                    }
                }
            });
        }

        $.getJSON(inventoryUrl('/stats/revenue') + `&start=${start}&end=${end}`, function (data) {
            showRevenues(data.revenues);
        });
    }

}