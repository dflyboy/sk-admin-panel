function getFormJSON($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        if (n['value'].length > 0) indexed_array[n['name']] = n['value'];
    });

    return JSON.stringify(indexed_array);
}

function showAddError(message) {
    $('#error-alert .message').text(message);
    $('#error-alert').show(200);
    $('#error-alert .card-action .close').one('click', function (e) {
        $('#error-alert').hide(200);
    });
}

function showAddSuccess() {
    $('#success-alert').show(200);
    $('#success-alert .card-action .close').one('click', function (e) {
        location.reload();
    });
}

async function initPage() {
    await Localize.loadNamespaces('inventory');
    Localize.localize();

    const url = (endpoint) => '/adder/' + endpoint;
    let product = {};

    const el = document.querySelector('.tabs');
    const tabs = M.Tabs.init(el, {});

    $('select').formSelect();

    function showConfirmData(data) {
        let view = $('<pre>').text(JSON.stringify(data, undefined, 4));
        $('#confirm-form .content').append(view);
    }

    function focusInput(name) {
        $(`input[name="${name}"]`).focus();
    }

    function setUpForms() {
        $('body[data-page="adder"] form').submit(function (e) {
            e.preventDefault();
        });

        //TODO: input validation

        $('#ean-form form').submit(function (e) {
            $.ajax({
                url: url('barcode'),
                type: 'POST',
                data: getFormJSON($(this)),
                contentType: 'application/json'
            }).done(function (data) {
                product = data;

                if (data.name) $('#name-form input[name="name"]').val(data.name);
                tabs.select('name-form');

                focusInput('name');
            });
        });

        $('#name-form form').submit(function (e) {
            //TODO: handle automatic flow (prevent submission)
            $.ajax({
                url: url('name'),
                type: 'POST',
                data: getFormJSON($(this)),
                contentType: 'application/json'
            }).done(function (data) {
                product = data;

                tabs.select('price-form');
                focusInput('price');
            });
        });

        $('#price-form form').submit(function (e) {
            $.ajax({
                url: url('price'),
                type: 'POST',
                data: getFormJSON($(this)),
                contentType: 'application/json'
            }).done(function (data) {
                product = data;

                tabs.select('quantity-form');
                focusInput('quantity');

            });
        });

        $('#quantity-form form').submit(function (e) {
            $.ajax({
                url: url('quantity'),
                type: 'POST',
                data: getFormJSON($(this)),
                contentType: 'application/json'
            }).done(function (data) {
                product = data;

                showConfirmData(data);
                tabs.select('confirm-form');
                $('#confirm-btn').focus();
            });
        });

        $('#confirm-btn').one('click', function (e) {
            e.preventDefault();
            let dataBody = {
                product,
                inventoryId: 1
            };
            $.ajax({
                url: url('confirm'),
                type: 'POST',
                data: JSON.stringify(dataBody),
                contentType: 'application/json'
            }).done(function (data) {
                if (data.error) {
                    showAddError(data.error);
                } else {
                    showAddSuccess();
                }
            });
        });

        focusInput('ean');

    }

    Localize.loadNamespaces('inventory').then(() => {
        setUpForms();
    });

}