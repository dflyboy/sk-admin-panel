/**
 * called after localization done
 */
function initPage() {
    $.getJSON(inventoryUrl('/list/sales'), function (data) {
        listSales(data.sales);
    });

    M.Collapsible.init($('.collapsible').get(0), {
        onOpenStart: addEntryTable
    });
    // $('.collapsible').collapsible();

    var elems = document.querySelectorAll('.datepicker');
    M.Datepicker.init(elems, {
        format: 'dd.mm.yyyy'
    });

    $('#filterForm').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: inventoryUrl('/list/sales'),
            type: 'GET',
            data: $(this).serialize(),
        }).done(function (data) {
            listSales(data.sales);
        });
    });
}

function listSales(positions) {
    var list = $('ul.sales-list');
    list.empty();

    $.each(positions, function (index, value) {
        addSaleEntry(list, value);
    });
}

var entryBody = '<div class="collapsible-body"><div class="info section"></div><table class="striped"><thead><tr><th data-i18n="product"></th><th data-i18n="amount"></th><th data-i18n="vat"></th><th data-i18n="unit_cost"></th><th class="right" data-i18n="sales:sum"></th></tr></thead><tbody></tbody></table></div>';

function addSaleEntry(list, sale) {
    var entry = $('<li>');
    entry.data('sale', sale.id);
    var header = $('<div class="collapsible-header">').appendTo(entry);
    entry.append($(entryBody).localize());
    var time = moment(sale.createdAt).calendar();
    header.append($('<i class="material-icons">shopping_cart</i>'));
    header.append($('<span>').text(time));
    header.append($('<span>').addClass('price').text(sale.total + '€'));
    list.append(entry);
}

function addEntryTable(elem) {
    var entry = $(elem);
    var saleId = entry.data('sale');
    var tbody = entry.find('tbody');
    tbody.empty();
    $.getJSON(inventoryUrl('/sale/' + saleId + '/list'), function (data) {
        $.each(data, function (index, item) {
            var tr = $('<tr>');
            tr.append($('<td>').text(item.product.name));
            tr.append($('<td>').addClass('min').text(item.quantity));
            tr.append($('<td>').addClass('min').text(Math.floor(item.product.tax * 100) + '%'));
            tr.append($('<td>').addClass('min').text(item.price + '€'));
            var sum = Math.floor(item.price * item.quantity * 100) / 100;
            tr.append($('<td>').addClass('min').text(sum + '€'));

            tbody.append(tr);
        });
    });
}