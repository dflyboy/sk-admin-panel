function getFormJSON($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        if (n['value'].length > 0) indexed_array[n['name']] = n['value'];
    });

    return JSON.stringify(indexed_array);
}

function updateInventory() {
    return new Promise((resolve, reject) => {
        $.getJSON(inventoryUrl('/list'), function (data) {
            var tbody = $('table.inventory').children('tbody');
            tbody.empty();
            var positions = data.positions;
            $.each(positions, function (index, value) {
                var pos = {
                    id: value.id,
                    quantity: value.quantity,
                    price: value.price,
                    product: {
                        id: value.product.id,
                        name: value.product.name,
                        gtin: value.product.gtin
                    }
                };
                var tr = $('<tr>');
                tr.append($('<td>').text(pos.id));
                tr.append($('<td>').text(pos.product.name));
                tr.append($('<td>').text(pos.product.gtin));
                tr.append($('<td>').text(pos.quantity));
                tr.append($('<td>').text(pos.price + '€').addClass('right'));

                tr.click(function () {
                    $('#changeForm input.name').val(pos.product.name);
                    $('#changeForm input.pid').val(pos.product.id);
                    $('#changeForm input.quantity').val(pos.quantity);
                    $('#changeForm input.price').val(pos.price);
                    $('#position-change-modal').modal('open');
                });

                tbody.append(tr);
            });
            resolve();
        });
    })
}

/**
 * called after localization done
 */
function initPage() {
    $('input#ean').characterCounter();
    $('.modal').modal();
    $('select').formSelect();

    function inlineFormResponse(data) {
        if (typeof data.error !== 'undefined') {
            var msg = '<strong>Fehler!</strong><p>' + data.error + '</p>';
            M.toast({
                html: msg,
                classes: 'error red'
            });
        } else {
            updateInventory();
            $('#position-change-modal').modal('close');
        }
    }

    updateInventory().then(Tablesaw.init);

    $('#addForm').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: inventoryUrl('/add'),
            type: 'POST',
            data: getFormJSON($(this)),
            contentType: 'application/json'
        }).done(inlineFormResponse);
    });

    $('#changeForm').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: inventoryUrl('/change'),
            type: 'POST',
            data: getFormJSON($(this)),
            contentType: 'application/json'
        }).done(inlineFormResponse);
    });


    $('#inputFilter').on('keyup', function () {
        var value = $(this).val().toLowerCase();
        $('table.inventory tbody tr').filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });

    $('#download-btn').on('click', function(e) {
        e.preventDefault();
        let url = inventoryUrl('/list/download');
        if($('#download-all')[0].checked) url += '&all=1';
        window.open(url, '_blank');
    });
}