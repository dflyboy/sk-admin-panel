function readCookie(cookieName) {
    var re = new RegExp('[; ]'+cookieName+'=([^\\s;]*)');
    var sMatch = (' '+document.cookie).match(re);
    if (cookieName && sMatch) return unescape(sMatch[1]);
    return '';
}

function inventoryUrl(url) {
    return `/inventory${url}?id=${window.inventoryId}`;
}

$(document).ready(function () {
    Localize.init($('body').data('page')).then(() => {
        Localize.localize();
        Localize.setupLanguageMenu();
        window.inventoryId = readCookie('storekeepers.inventoryId') || null;
        $('.sidenav').sidenav();
        initPage();
    });
});