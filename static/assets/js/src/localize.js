class Localize {
    static async init(...scopes) {
        let ns = ['common'];
        if (scopes) ns.push(scopes);
        const t = await i18next
            .use(i18nextBrowserLanguageDetector)
            .use(i18nextLocalStorageBackend)
            .use(I18nextFetchBackend)
            .init({
                fallbackLng: 'de',
                ns,
                whitelist: ['de', 'fr'],
                defaultNS: 'common',
                debug: false,
                backend: {
                    backends: [
                        i18nextLocalStorageBackend, // primary
                        I18nextFetchBackend // fallback
                    ],
                    backendOptions: [{
                        // prefix for stored languages
                        prefix: 'i18next_fv_',
                        expirationTime: 7 * 24 * 60 * 60 * 1000,
                        versions: {
                            de: 'v1.0',
                            en: 'v1.0',
                            zh: 'v1.0'
                        },
                    }, {
                        loadPath: '/assets/locales/{{lng}}/{{ns}}.json' // xhr load path for my own fallback
                    }],
                    loadPath: '/assets/locales/{{lng}}/{{ns}}.json'
                }
            });
        jqueryI18next.init(i18next, $, {
            tName: 't', // --> appends $.t = i18next.t
            i18nName: 'i18n', // --> appends $.i18n = i18next
            handleName: 'localize', // --> appends $(selector).localize(opts);
            selectorAttr: 'data-i18n', // selector for translating elements
            targetAttr: 'i18n-target', // data-() attribute to grab target element to translate (if different than itself)
            optionsAttr: 'i18n-options', // data-() attribute that contains options, will load/set if useOptionsAttr = true
            useOptionsAttr: false, // see optionsAttr
            parseDefaultValueFromContent: true // parses default values from content ele.val or ele.text
        });
        moment.locale(i18next.language);
        return t;
    }

    static async loadNamespaces(ns) {
        await i18next.loadNamespaces(ns);
    }

    static async changeLanguage(lng) {
        await i18next.changeLanguage(lng);
        moment.locale(lng);
        this.localize();
    }

    static setupLanguageMenu() {
        $('.lng.dropdown-trigger').dropdown();
        $('#dropdown-lng a').click(function () {
            const lng = $(this).data('lng');
            Localize.changeLanguage(lng);
        });
    }

    static localizeContent() {
        $('body .container').localize();
    }

    static localizeNavbar() {
        $('body > nav').localize();
    }

    static localize() {
        this.localizeContent();
        this.localizeNavbar();
    }
}