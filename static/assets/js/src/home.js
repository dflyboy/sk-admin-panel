function initParallax() {
    const width = $(window).width();
    const dir = '/assets/img/';
    let res;
    if (width <= 992) {
        res = '992';
    } else if (width <= 1920) {
        res = '1920';
    } else {
        res = 'max';
    }
    $('#bg1').attr('src', dir + res + '/bg1.jpg');
    $('.parallax').parallax();
}

class Scrollshow {
    constructor() {
        Scrollshow.sections = $('.section.scrollshow');
        Scrollshow.effect = {
            effect: 'drop',
            // direction: 'down',
            duration: 800
        };
        Scrollshow.offset = 50;
        $(window).on('resize', this.getHeight);
        this.getHeight();
    }

    getHeight() {
        Scrollshow.viewPortHeight = (window.innerHeight || document.documentElement.clientHeight);
    }

    listen() {
        this.hideAll();
        $(window).on('scroll', this.scrollHandler);
    }

    hideAll() {
        Scrollshow.sections.each(function() {
            this.style['min-height'] = ((this.clientHeight / 2) + Scrollshow.offset) + 'px';
            $(this).children().hide();
        });
    }

    scrollHandler(e) {
        Scrollshow.sections.filter(':not(.active)').each(function () {
            const top = this.getBoundingClientRect().top;
            if (top - Scrollshow.offset <= Scrollshow.viewPortHeight) {
                const $this = $(this);
                $this.addClass('active');
                setTimeout(() => {
                    $this.children().show(Scrollshow.effect);
                    setTimeout(() => {
                        $this.css('min-height', null);
                    }, Scrollshow.effect.duration);
                }, 10);
            }
        });
    }
}

function initPage() {

    $('.sidenav').sidenav();
    $('.parallax').parallax();

    function errorToast(text) {
        var msg = `<strong>${i18next.t('error')}! </strong><p>${text}</p>`;
        M.toast({
            html: msg,
            classes: 'error red'
        });
    }

    function successToast() {
        M.toast({
            html: i18next.t('home:message.success'),
            classes: 'green darken-1'
        });
    }

    $('#contact-form').on('submit', function (e) {
        e.preventDefault();
        const tel = $('#input-tel');
        const email = $('#input-email');

        if (email.hasClass('invalid')) {
            errorToast('Email-Adresse ungültig');
        } else if (!(tel.val() || email.val())) {
            errorToast('Keine Daten angegeben');
        } else {
            $.ajax({
                url: $(this).attr('action'),
                method: 'post',
                data: $(this).serialize()
            })
                .done(data => {
                    if (data.success) {
                        successToast();
                    } else {
                        errorToast(data.error || '');
                    }
                });
        }
    });

    const scrollshow = new Scrollshow();
    scrollshow.listen();
}