const path = require("path");
const fs = require("fs");

const primary = "de";
const locales = ["fr"];

let primaryFiles = fs.readdirSync(path.join(__dirname, primary));
primaryFiles.forEach((file, i, a) => primaryFiles[i] = path.resolve(path.join(__dirname, primary), file));

let nspaces = [];
primaryFiles.forEach((file, i) => nspaces[i] = path.basename(file, ".json"));

console.log(primaryFiles, nspaces);

function addMisssing(primary, secondary) {
    let res = {};
    Object.assign(res, secondary)
    for (let [key, value] of Object.entries(primary)) {
        switch (typeof secondary[key]) {
        case "undefined":
            res[key] = value;
            break;
        case "object":
            res[key] = (secondary === undefined) ? value : addMisssing(value, res[key]);
            break;
        case "string":
            res[key] = secondary[key];
            break;
        }
    }
    return res;
}

nspaces.forEach((ns, i) => {
    const primEntries = JSON.parse(fs.readFileSync(primaryFiles[i]));
    for (let lng of locales) {
        const dir = path.join(__dirname, lng);
        const nsFile = path.join(dir, ns + ".json");
        if (!fs.existsSync(nsFile)) fs.copyFileSync(primaryFiles[i], nsFile);
        else {
            let old = JSON.parse(fs.readFileSync(nsFile));
            const newNs = addMisssing(primEntries, old);
            console.log(nsFile);
            console.dir(newNs);
            fs.writeFileSync(nsFile, JSON.stringify(newNs, null, 4));
        }
    }
});